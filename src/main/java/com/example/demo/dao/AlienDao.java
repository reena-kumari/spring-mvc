package com.example.demo.dao;

import java.util.List;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import com.example.demo.model.Alien;

import javax.transaction.Transactional;

//import com.mysql.cj.xdevapi.SessionFactory;


@Component
public class AlienDao {

	@Autowired
	private SessionFactory sessionFactory;

	@Transactional
	public List<Alien> getAliens() {
		Session session = sessionFactory.getCurrentSession();
		//Query query = session.createQuery("from Alien"); 

		// List<Weather> list = query.list(); 
		List<Alien> aliens = session.createQuery("from Alien", Alien.class).list();

		return aliens;
	}

	@Transactional
	public void save(Alien a) {
		Session session = sessionFactory.getCurrentSession();
		session.save(a);
	}

	@Transactional
	public Alien getAlien(int aid) {
		Session session = sessionFactory.getCurrentSession();
		return session.get(Alien.class, aid);
	}

}
