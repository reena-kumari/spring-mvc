package com.example.demo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.example.demo.dao.AlienDao;
import com.example.demo.model.Alien;

@Controller
public class HomePageController {
	
	@Autowired
	private AlienDao dao;
	
	@ModelAttribute
	public void modelData(Model m) 
	{
	m.addAttribute("name","Aliens");
	}
	
	@RequestMapping("/")
	public String home() {
		System.out.println("hello");
		return "index"; //we just has to mention the filename, dispature servlet is responsible to open the file
	}
	
	//working with HTTPservletRequest
//	@RequestMapping("add")
//	public String add(HttpServletRequest req) {
//		
//		int i = Integer.parseInt(req.getParameter("num1"));
//		int j = Integer.parseInt(req.getParameter("num2"));
//		
//		int num3 = i+j;
//		
//		HttpSession session = req.getSession();
//		session.setAttribute("num3", num3);
//		return "result.jsp";
//	}
	
	//getting data with RequestParam
//	@RequestMapping("add")
//	public String add(@RequestParam("num1") int i,  @RequestParam("num2") int j,HttpSession session) {
//		
//		int num3 = i+j;
//		session.setAttribute("num3", num3);
//		return "result.jsp";
//	}
	
	//returning data back with ModalAndView
//	@RequestMapping("add")
//	public ModelAndView add(@RequestParam("num1") int i,  @RequestParam("num2") int j) {
//		
//		ModelAndView mv = new ModelAndView();
//		mv.setViewName("result.jsp"); //file will be looked in webapp
//		int num3 = i+j;
//		mv.addObject("num3", num3);
//		return mv;
//	}
	
	@RequestMapping("add")
	public ModelAndView add(@RequestParam("num1") int i,  @RequestParam("num2") int j) {
		
		ModelAndView mv = new ModelAndView();
		mv.setViewName("result"); //file is inside webapp/views and extension is jsp, and these are coming from application properties file
		int num3 = i+j;
		mv.addObject("num3", num3);
		return mv;
	}
	
	//Input request param will be assigned automatically to Java Class, if param has same name
	@RequestMapping("show1")
	public String addAlien(Alien a, Model m)
	{
		m.addAttribute("alien", a);
		return "userInfo";
		
	}
	
	//Since same alien object has assigned back to UI, it can be handle by spring   
	//That is handle by @ModelAttribute annotation
	//model attribute assigned for this is 'alien'(not a1)
	//If we want different name in UI for model then public String addAlien(@ModelAttribute("a1") Alien a) 
	@RequestMapping("show")
	public String addAlien(@ModelAttribute("result") Alien a)
	{
		dao.save(a);
		return "userInfo";
		
	}
	
	@GetMapping("aliens")
	public String getAliens(Model m)
	{
		m.addAttribute("result", dao.getAliens());
		return "userInfo";
		
	}
	
	@GetMapping("alien")
	public String getAlien(@RequestParam int aid, Model m)
	{
		m.addAttribute("result", dao.getAlien(aid));
		return "userInfo";
		
	}
}
